/**
 * 商品换货相关API
 */

import request from '@/utils/request'

/**
 * 出库单商品明细
 * @param params
 */
export function getWarehouseOutProduct(params) {
  return request({
    url: 'admin/erp/warehouseOutItem',
    method: 'get',
    loaidng: false,
    params
  })
}
