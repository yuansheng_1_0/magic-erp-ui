/**
 * 快递模板相关API
 */

import request from '@/utils/request'
import { api } from '~/ui-domain'

/**
 * 运费模版列表
 */
export function getTplList(businessType, keyWord) {
  return request({
    url: `/admin/trade/ship-templates?key_word=${keyWord}&business_type=${businessType}`,
    method: 'get',
    loading: false
  })
}

/**
 * 查询单个运费模版
 * @param id
 */
export function getSimpleTpl(id) {
  return request({
    url: `/admin/trade/ship-templates/${id}`,
    method: 'get',
    loading: false
  })
}

/**
 * 删除快递模板
 * @param ids
 */
export function deleteExpressMould(ids) {
  return request({
    url: `/admin/trade/ship-templates/${ids}`,
    method: 'delete'
  })
}

/**
 * 更新运费模板
 * @param id
 * @param params
 * @returns {Promise<any>}
 */
export function saveExpressMould(id, params) {
  return request({
    url: `/admin/trade/ship-templates/${id}`,
    headers: { 'Content-Type': 'application/json' },
    method: 'put',
    data: params
  })
}

/**
 * 添加运费模版
 * @param params
 * @returns {Promise<any>}
 */
export function addExpressMould(params) {
  return request({
    url: '/admin/trade/ship-templates',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}
/**
 * 校验包邮工具
 * @param params
 * @returns {Promise<any>}
 */
export function shipTemplatesCheck(params) {
  return request({
    url: `/admin/trade/ship-templates/check`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}
/**
 * 获取树形选择器地区信息
 */
export function getAreaList() {
  return request({
    url: `${api.admin}/regions/depth/3`,
    method: 'get'
  })
}

