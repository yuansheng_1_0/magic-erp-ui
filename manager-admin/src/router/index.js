import Vue from 'vue'
import Router from 'vue-router'
import Layout from '@/views/layout/Layout'
import RouterView from '@/views/router-view'
import mixin from '@/utils/mixin'

const alias = require('ui-domain/alias')

const routerPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error => error)
}

Vue.use(Router)

export const constantRouterMap = [
  { path: '/login', component: () => import('@/views/login/index'), hidden: true },
  { path: '/franchise/login', component: () => import('@/views/login/index'), hidden: true },
  { path: '/404', component: () => import('@/views/errorPage/404'), hidden: true },
  { path: '/401', component: () => import('@/views/errorPage/401'), hidden: true }
]

export default new Router({
  mode: 'history',
  base: alias.admin ? `/${alias.admin}/` : undefined,
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

const asyncRouters = [
  // 首页
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    name: 'dashboard',
    children: [
      { path: 'dashboard', component: () => import('@/views/dashboard/index'), name: 'dashboard', meta: { title: 'home', icon: 'dashboard' } }
    ]
  },
  // 采购管理
  {
    path: '/purchase',
    component: Layout,
    redirect: '/purchase/purchase-plan',
    name: 'purchase',
    meta: {
      title: 'purchase',
      icon: 'purchase'
    },
    children: [
      { path: 'purchase-plan', component: () => import('@/views/purchase/purchasePlan'), name: 'purchasePlan', meta: { title: 'purchasePlan' } },
      { path: 'purchase-plan-add', component: () => import('@/views/purchase/purchasePlanAdd'), name: 'purchasePlanAdd', meta: { title: 'purchasePlanAdd', noCache: true }, hidden: true },
      { path: 'purchase-plan-edit/:id?', component: () => import('@/views/purchase/purchasePlanAdd'), name: 'purchasePlanEdit', hidden: true, meta: { title: 'purchasePlanEdit' } },
      { path: 'purchase-plan-detail/:id?', component: () => import('@/views/purchase/purchasePlanAdd'), name: 'purchasePlanDetail', hidden: true, meta: { title: 'purchasePlanDetail' } },
      { path: 'purchase-contract', component: () => import('@/views/purchase/purchaseContract'), name: 'purchaseContract', meta: { title: 'purchaseContract' } },
      { path: 'purchase-contract-add', component: () => import('@/views/purchase/purchaseContractAdd'), name: 'purchaseContractAdd', meta: { title: 'purchaseContractAdd', noCache: true }, hidden: true },
      { path: 'purchase-contract-edit/:id?', component: () => import('@/views/purchase/purchaseContractAdd'), name: 'purchaseContractEdit', meta: { title: 'purchaseContractEdit', noCache: true }, hidden: true },
      { path: 'purchase-contract-detail/:id?', component: () => import('@/views/purchase/purchaseContractAdd'), name: 'purchaseContractDetail', meta: { title: 'purchaseContractDetail', noCache: true }, hidden: true },
      { path: 'purchase-warehousing', component: () => import('@/views/purchase/purchaseWarehousing'), name: 'purchaseWarehousing', meta: { title: 'purchaseWarehousing' } },
      { path: 'purchase-warehousing-add', component: () => import('@/views/purchase/purchaseWarehousingAdd'), name: 'purchaseWarehousingAdd', meta: { title: 'purchaseWarehousingAdd', noCache: true }, hidden: true },
      { path: 'purchase-warehousing-edit/:id?', component: () => import('@/views/purchase/purchaseWarehousingAdd'), name: 'purchaseWarehousingEdit', meta: { title: 'purchaseWarehousingEdit', noCache: true }, hidden: true },
      { path: 'purchase-warehousing-audit/:id?', component: () => import('@/views/purchase/purchaseWarehousingAdd'), name: 'purchaseWarehousingAudit', meta: { title: 'purchaseWarehousingAudit', noCache: true }, hidden: true },
      { path: 'purchase-warehousing-detail/:id?', component: () => import('@/views/purchase/purchaseWarehousingAdd'), name: 'purchaseWarehousingDetail', meta: { title: 'purchaseWarehousingDetail', noCache: true }, hidden: true },
      { path: 'supplier-returns', component: () => import('@/views/purchase/supplierReturns'), name: 'supplierReturns', meta: { title: 'supplierReturns' } },
      { path: 'supplier-returns-add', component: () => import('@/views/purchase/supplierReturnsAdd'), name: 'supplierReturnsAdd', meta: { title: 'supplierReturnsAdd', noCache: true }, hidden: true },
      { path: 'supplier-returns-edit/:id?', component: () => import('@/views/purchase/supplierReturnsAdd'), name: 'supplierReturnsEdit', meta: { title: 'supplierReturnsEdit', noCache: true }, hidden: true },
      { path: 'supplier-returns-audit/:id?', component: () => import('@/views/purchase/supplierReturnsAdd'), name: 'supplierReturnsAudit', meta: { title: 'supplierReturnsAudit', noCache: true }, hidden: true },
      { path: 'supplier-returns-detail/:id?', component: () => import('@/views/purchase/supplierReturnsAdd'), name: 'supplierReturnsDetail', meta: { title: 'supplierReturnsDetail', noCache: true }, hidden: true },
      { path: 'return-order-audit', component: () => import('@/views/purchase/supplierReturns'), name: 'returnOrderAudit', meta: { title: 'returnOrderAudit', noCache: true }, hidden: true },
      { path: 'supplier-settlement', component: () => import('@/views/purchase/supplierSettlement'), name: 'supplierSettlement', meta: { title: 'supplierSettlement' } },
      { path: 'settlement-deatil/:id?', component: () => import('@/views/purchase/settlementDeatil'), name: 'settlementDeatil', meta: { title: 'settlementDeatil', noCache: true }, hidden: true }
    ]
  },
  // 仓库管理
  {
    path: '/stock',
    component: Layout,
    redirect: '/stock/order',
    name: 'stock',
    meta: { title: 'stock', icon: 'stock' },
    children: [
      { path: 'goods-transfer', component: () => import('@/views/stock/goodsTransfer'), name: 'goodsTransfer', meta: { title: 'goodsTransfer' } },
      { path: 'goods-transfer-add', component: () => import('@/views/stock/goodsTransferAdd'), name: 'goodsTransferAdd', meta: { title: 'goodsTransferAdd', noCache: true }, hidden: true },
      { path: 'goods-transfer-edit', component: () => import('@/views/stock/goodsTransferAdd'), name: 'goodsTransferEdit', meta: { title: 'goodsTransferEdit', noCache: true }, hidden: true },
      { path: 'goods-transfer-detail', component: () => import('@/views/stock/goodsTransferAdd'), name: 'goodsTransferDetail', meta: { title: 'goodsTransferDetail', noCache: true }, hidden: true },
      { path: 'goods-lend', component: () => import('@/views/stock/goodsLend'), name: 'goodsLend', meta: { title: 'goodsLend' } },
      { path: 'goods-lend-add', component: () => import('@/views/stock/goodsLendAdd'), name: 'goodsLendAdd', meta: { title: 'goodsLendAdd', noCache: true }, hidden: true },
      { path: 'goods-lend-edit/:id', component: () => import('@/views/stock/goodsLendAdd'), name: 'goodsLendEdit', meta: { title: 'goodsLendEdit', noCache: true }, hidden: true },
      { path: 'goods-lend-detail/:id', component: () => import('@/views/stock/goodsLendAdd'), name: 'goodsLendDetail', meta: { title: 'goodsLendDetail', noCache: true }, hidden: true },
      { path: 'goods-lend-return/:id', component: () => import('@/views/stock/goodsLendAdd'), name: 'goodsLendReturn', meta: { title: 'goodsLendReturn', noCache: true }, hidden: true },
      { path: 'goods-exchange-list', component: () => import('@/views/stock/goodsExchange/goodsExchange'), name: 'goodsExchange', meta: { title: 'goodsExchange' } },
      { path: 'goods-exchange-add', component: () => import('@/views/stock/goodsExchange/goodsExchangeAdd'), name: 'goodsExchangeAdd', meta: { title: 'goodsExchangeAdd', noCache: true }, hidden: true },
      { path: 'goods-exchange-edit/:id?', component: () => import('@/views/stock/goodsExchange/goodsExchangeAdd'), name: 'goodsExchangeEdit', meta: { title: 'goodsExchangeEdit', noCache: true }, hidden: true },
      { path: 'goods-exchange-audit/:id?', component: () => import('@/views/stock/goodsExchange/goodsExchangeAdd'), name: 'goodsExchangeAudit', meta: { title: 'goodsExchangeAudit', noCache: true }, hidden: true },
      { path: 'goods-exchange-detail/:id?', component: () => import('@/views/stock/goodsExchange/goodsExchangeAdd'), name: 'goodsExchangeDetail', meta: { title: 'goodsExchangeDetail', noCache: true }, hidden: true },
      { path: 'goods-inventory-list', component: () => import('@/views/stock/goodsInventory/goodsInventory'), name: 'goodsInventory', meta: { title: 'goodsInventory' } },
      { path: 'goods-inventory-add/:id?', component: () => import('@/views/stock/goodsInventory/goodsInventoryAdd'), name: 'goodsInventoryAdd', meta: { title: 'goodsInventoryAdd' }, hidden: true },
      { path: 'goods-inventory-edit/:id?', component: () => import('@/views/stock/goodsInventory/goodsInventoryAdd'), name: 'goodsInventoryEdit', meta: { title: 'goodsInventoryEdit', noCache: true }, hidden: true },
      { path: 'goods-inventory-audit/:id?', component: () => import('@/views/stock/goodsInventory/goodsInventoryAdd'), name: 'goodsInventoryAudit', meta: { title: 'goodsInventoryAudit', noCache: true }, hidden: true },
      { path: 'goods-inventory-detail/:id?', component: () => import('@/views/stock/goodsInventory/goodsInventoryAdd'), name: 'goodsInventoryDetail', meta: { title: 'goodsInventoryDetail', noCache: true }, hidden: true },
      { path: 'goods-inventoryLoss-list', component: () => import('@/views/stock/goodsInventoryLoss/goodsInventoryLoss'), name: 'goodsInventoryLoss', meta: { title: 'goodsInventoryLoss' } },
      { path: 'goods-inventoryLoss-add/:id?', component: () => import('@/views/stock/goodsInventoryLoss/goodsInventoryLossAdd'), name: 'goodsInventoryLossAdd', meta: { title: 'goodsInventoryLossAdd' }, hidden: true },
      { path: 'goods-inventoryLoss-edit/:id?', component: () => import('@/views/stock/goodsInventoryLoss/goodsInventoryLossAdd'), name: 'goodsInventoryLossEdit', meta: { title: 'goodsInventoryLossEdit', noCache: true }, hidden: true },
      { path: 'goods-inventoryLoss-audit/:id?', component: () => import('@/views/stock/goodsInventoryLoss/goodsInventoryLossAdd'), name: 'goodsInventoryLossAudit', meta: { title: 'goodsInventoryLossAudit', noCache: true }, hidden: true },
      { path: 'goods-inventoryLoss-detail/:id?', component: () => import('@/views/stock/goodsInventoryLoss/goodsInventoryLossAdd'), name: 'goodsInventoryLossDetail', meta: { title: 'goodsInventoryLossDetail', noCache: true }, hidden: true },
      { path: 'goods-understock', component: () => import('@/views/stock/goodsUnderstock'), name: 'goodsUnderstock', meta: { title: 'goodsUnderstock' } }
      // { path: 'push-record', component: () => import('@/views/stock/pushRecord'), name: 'pushRecord', meta: { title: 'pushRecord' }}
    ]
  },
  // 库存报表
  {
    path: '/statistics',
    component: Layout,
    redirect: '/statistics/exchange-goods',
    name: 'statistics',
    meta: {
      title: 'statistics',
      icon: 'order-manage'
    },
    children: [
      { path: 'stock-statistics', component: () => import('@/views/statistics/stockStatistics'), name: 'stockStatistics', meta: { title: 'stockStatistics' } },
      { path: 'stock-cost-statistics', component: () => import('@/views/statistics/stockCostStatistics'), name: 'stockCostStatistics', meta: { title: 'stockCostStatistics' } },
      { path: 'purchase-plan-statistics', component: () => import('@/views/statistics/purchasePlanStatistics'), name: 'purchasePlanStatistics', meta: { title: 'purchasePlanStatistics' } },
      { path: 'statistics-of-goods', component: () => import('@/views/statistics/statisticsOfGoods'), name: 'statisticsOfGoods', meta: { title: 'statisticsOfGoods' }, hidden: true },
      { path: 'supplier-returns-statistics', component: () => import('@/views/statistics/supplierReturnsStatistics'), name: 'supplierReturnsStatistics', meta: { title: 'supplierReturnsStatistics' } },
      { path: 'warehouse-out-statistics', component: () => import('@/views/statistics/warehouseOutStatistics'), name: 'warehouseOutStatistics', meta: { title: 'warehouseOutStatistics' } },
      { path: 'order-return-statistics', component: () => import('@/views/statistics/orderRetrunStatistics'), name: 'orderRetrunStatistics', meta: { title: 'orderRetrunStatistics' } },
      { path: 'allot-statistics', component: () => import('@/views/statistics/allotStatistics'), name: 'allotStatistics', meta: { title: 'allotStatistics' } },
      { path: 'stock-update-statistics', component: () => import('@/views/statistics/stockUpdateStatistics'), name: 'stockUpdateStatistics', meta: { title: 'stockUpdateStatistics' } },
      { path: 'goods-exchange-statistics', component: () => import('@/views/statistics/goodsExchangeStatistics'), name: 'goodsExchangeStatistics', meta: { title: 'goodsExchangeStatistics' } }
    ]
  },
  // 销售管理
  {
    path: '/sales',
    component: Layout,
    redirect: '/stock/order',
    name: 'sales',
    meta: { title: 'sales', icon: 'shoppingCard' },
    children: [
      { path: 'order', component: () => import('@/views/stock/order'), name: 'order', meta: { title: 'order', noCache: true } },
      { path: 'order-add', component: () => import('@/views/stock/orderAdd'), name: 'orderAdd', meta: { title: 'orderAdd' }, hidden: true },
      { path: 'order-edit/:id?', component: () => import('@/views/stock/orderAdd'), name: 'orderEdit', meta: { title: 'orderEdit' }, hidden: true },
      { path: 'order-detail/:id?', component: () => import('@/views/stock/orderAdd'), name: 'orderDetail', meta: { title: 'orderDetail' }, hidden: true },
      { path: 'warehouse-out', component: () => import('@/views/stock/warehouseOut'), name: 'warehouseOut', meta: { title: 'warehouseOut' } },
      { path: 'order-outbound/:sn?', component: () => import('@/views/stock/orderOutbound'), name: 'orderOutbound', meta: { title: 'orderOutbound', noCache: true }, hidden: true },
      { path: 'order-outbound/:id?', component: () => import('@/views/stock/orderOutbound'), name: 'orderOutboundDetail', meta: { title: 'orderOutbound', noCache: true }, hidden: true },
      { path: 'order-return', component: () => import('@/views/stock/orderReturn'), name: 'orderReturn', meta: { title: 'orderReturn' } },
      { path: 'order-return-add/:id?', component: () => import('@/views/stock/orderReturnAdd'), name: 'orderReturnAdd', hidden: true, meta: { title: 'orderReturnAdd' } },
      { path: 'order-return-edit/:id?', component: () => import('@/views/stock/orderReturnAdd'), name: 'orderReturnEdit', hidden: true, meta: { title: 'orderReturnEdit' } },
      { path: 'order-return-audit/:id?', component: () => import('@/views/stock/orderReturnAdd'), name: 'orderReturnAudit', hidden: true, meta: { title: 'orderReturnAudit' } },
      { path: 'order-return-detail/:id?', component: () => import('@/views/stock/orderReturnAdd'), name: 'orderReturnDetail', hidden: true, meta: { title: 'orderReturnDetail' } }
    ]
  },
  // 零售管理
  {
    path: '/retail',
    component: Layout,
    redirect: '/retail/order',
    name: 'retail',
    meta: { title: 'retail', icon: 'shoppingCard' },
    children: [
      { path: 'order', component: () => import('@/views/stock/order'), name: 'retailOrder', meta: { title: 'retailOrder', noCache: true } },
      { path: 'order-add', component: () => import('@/views/stock/orderAdd'), name: 'orderAdd', meta: { title: 'orderAdd' }, hidden: true },
      { path: 'order-edit/:id?', component: () => import('@/views/stock/orderAdd'), name: 'orderEdit', meta: { title: 'orderEdit' }, hidden: true },
      { path: 'order-detail/:id?', component: () => import('@/views/stock/orderAdd'), name: 'orderDetail', meta: { title: 'orderDetail' }, hidden: true },
      { path: 'order-return', component: () => import('@/views/stock/orderReturn'), name: 'retailOrderReturn', meta: { title: 'retailOrderReturn' } },
      { path: 'order-return-add/:id?', component: () => import('@/views/stock/orderReturnAdd'), name: 'orderReturnAdd', hidden: true, meta: { title: 'orderReturnAdd' } },
      { path: 'order-return-edit/:id?', component: () => import('@/views/stock/orderReturnAdd'), name: 'orderReturnEdit', hidden: true, meta: { title: 'orderReturnEdit' } },
      { path: 'order-return-audit/:id?', component: () => import('@/views/stock/orderReturnAdd'), name: 'orderReturnAudit', hidden: true, meta: { title: 'orderReturnAudit' } },
      { path: 'order-return-detail/:id?', component: () => import('@/views/stock/orderReturnAdd'), name: 'orderReturnDetail', hidden: true, meta: { title: 'orderReturnDetail' } }

    ]
  },
  // 商品管理
  {
    path: '/goods',
    component: Layout,
    redirect: '/product-file',
    name: 'goods',
    meta: {
      title: 'goods',
      icon: 'goods-manage'
    },
    children: [
      { path: 'goods-list', component: () => import('@/views/basicSetting/goods/index.vue'), name: 'goodsList', meta: { title: 'goodsList' } },
      { path: 'goods-add', component: () => import('@/views/basicSetting/goods/editForm/index'), name: 'goodsAddForm', meta: { title: 'goodsAddForm', noCache: true }, hidden: true },
      { path: 'spec-list', component: () => import('@/views/basicSetting/specList'), name: 'specList', meta: { title: 'specList' }},
      { path: 'goods-edit/:id?', component: () => import('@/views/basicSetting/goods/editForm/index'), name: 'goodsEditForm', meta: { title: 'goodsEditForm', noCache: true }, hidden: true },
      { path: 'brand', component: () => import('@/views/basicSetting/brand'), name: 'brand', meta: { title: 'brand' } },
      { path: 'category-params/:id', component: () => import('@/views/basicSetting/categoryParams'), name: 'categoryParams', hidden: true, meta: { title: 'categoryParams', noCache: true }},
      { path: 'tag-manage', component: () => import('@/views/basicSetting/goodsTag/tagManage'), name: 'tagManage', meta: { title: 'tagManage' }},
      { path: 'tag-add/:tag_id', component: () => import('@/views/basicSetting/goodsTag/tagAdd'), name: 'tagAdd', meta: { title: 'tagAdd' }, hidden: true }
    ]
  },
  // 财务管理
  {
    path: '/finance',
    component: Layout,
    redirect: '/finance/income',
    name: 'finance',
    meta: {
      title: 'finance',
      icon: 'money'
    },
    children: [
      { path: 'income-list', component: () => import('@/views/finance/income'), name: 'financeIncomeList', meta: { title: 'financeIncome' } },
      { path: 'expand-list', component: () => import('@/views/finance/expand'), name: 'financeExpandList', meta: { title: 'financeExpand' } }
    ]
  },
  // 基础设置
  {
    path: '/basic-setting',
    component: Layout,
    redirect: '/setting/system',
    name: 'basicSetting',
    meta: {
      title: 'basicSetting',
      icon: 'member-manage'
    },
    children: [
      { path: 'supplier', component: () => import('@/views/basicSetting/supplier'), name: 'supplier', meta: { title: 'supplier' } },
      { path: 'enterprise', component: () => import('@/views/basicSetting/enterprise'), name: 'enterprise', meta: { title: 'enterprise' } },
      { path: 'marketing-manager', component: () => import('@/views/basicSetting/marketingManager'), name: 'marketingManager', meta: { title: 'marketingManager' } },
      { path: 'warehouse', component: () => import('@/views/basicSetting/warehouse'), name: 'warehouse', meta: { title: 'warehouse' } },
      { path: 'number-rule', component: () => import('@/views/basicSetting/numberRule'), name: 'numberRule', meta: { title: 'numberRule' } },
      { path: 'member', component: () => import('@/views/basicSetting/member/member'), name: 'member', meta: { title: 'member' } },
      { path: 'collecting-account', component: () => import('@/views/basicSetting/collectingAccount'), name: 'collectingAccount', meta: { title: 'collectingAccount' } },
      { path: 'regional-management', component: () => import('@/views/setting/payment-and-delivery/regionalManagement'), name: 'regionalManagementSettings', meta: { title: 'regionalManagement' } },
      { path: 'express', component: () => import('@/views/setting/payment-and-delivery/express'), name: 'expressSettings', meta: { title: 'expressSettings' } },
      { path: 'store', component: () => import('@/views/basicSetting/store'), name: 'store', meta: { title: 'store' } },
      { path: 'freight-template', component: () => import('@/views/basicSetting/freightTemplate'), name: 'freightTemplate', meta: { title: 'freightTemplate' }}
    ]
  },
  // 系统管理
  {
    path: '/setting',
    component: Layout,
    redirect: '/setting/administrator-manage',
    name: 'setting',
    meta: {
      title: 'setting',
      icon: 'setting-manage'
    },
    children: [
      {
        path: '/setting/system-params',
        component: () => import('@/views/setting/shop-settings/index'),
        redirect: '/setting/system-params/system',
        name: 'systemParams',
        meta: { title: 'systemParams' },
        children: [
          { path: 'system', component: () => import('@/views/setting/shop-settings/systemSettings'), name: 'systemSettings', meta: { title: 'systemSettings' } },
          { path: 'storage-solution', component: () => import('@/views/setting/shop-settings/storageSolution'), name: 'storageSolution', meta: { title: 'storageSolution' } },
          { path: 'dict-data', component: () => import('@/views/setting/tool-manage/dictData'), name: 'dictData', meta: { title: 'dictData' } },
          { path: 'system-logs', component: () => import('@/views/setting/systemLogs'), name: 'systemLogs', meta: { title: 'systemLogs' } }
        ]
      },
      {
        path: '/setting/oauth2',
        component: () => import('@/views/setting/shop-settings/index'),
        redirect: '/setting/oauth2/client',
        name: 'oauth2',
        meta: { title: 'oauth2' },
        children: [
          { path: 'client', component: () => import('@/views/setting/oauth2/client'), name: 'oauth2Client', meta: { title: 'oauth2Client' } },
          { path: 'open-api', component: () => import('@/views/setting/oauth2/openApi'), name: 'oauth2OpenApi', meta: { title: 'oauth2OpenApi' } },
          { path: 'token/:clientId', component: () => import('@/views/setting/oauth2/token'), name: 'oauth2Token', meta: { title: 'oauth2Token' }, hidden: true }
        ]
      },
      { path: 'administrator-manage', component: () => import('@/views/setting/auth-settings/administratorManage'), name: 'administratorManage', meta: { title: 'administratorManage' } },
      { path: 'role-manage', component: () => import('@/views/setting/auth-settings/roleManage'), name: 'roleManage', meta: { title: 'roleManage' } },
      { path: 'role-permission/:id(\\d+)', component: () => import('@/views/setting/auth-settings/rolePermission'), name: 'roleManageEdit', hidden: true, meta: { title: 'rolePermission', noCache: true } },
      { path: 'role-permission', component: () => import('@/views/setting/auth-settings/rolePermission'), name: 'roleManageAdd', hidden: true, meta: { title: 'rolePermission', noCache: true } },
      { path: 'menu-manage', component: () => import('@/views/setting/tool-manage/menuManage'), name: 'menuManage', meta: { title: 'menuManage' } },
      { path: 'branch', component: () => import('@/views/setting/tool-manage/branch'), name: 'branch', meta: { title: 'branch' } },
      { path: 'station', component: () => import('@/views/setting/tool-manage/station'), name: 'station', meta: { title: 'station' } }
    ]
  },
  { path: '*', redirect: '/404', hidden: true }
]

export const asyncRouterMap = asyncRouters
