# MagicErp

## ERP简介

MagicErp是使用java语言开发，基于SpringBoot2.X架构体系构建的一套erp系统，主要包含采购管理、仓库管理、销售管理、商品管理、库存报表、基础配置和系统配置等功能，细节上包含采购入库、订单销售、出库发货、库存盘点、库存报损、商品调拨、库存预警等基础进销存功能，同时可以根据不同的用户角色展示不同的数据以及按钮权限。致力于为广大企业提供高效便捷的ERP库存管理系统。

## api仓库地址

https://gitee.com/javastore/magic-erp.git

## 演示

### 演示地址

https://www.magicerp.cloud

账号：superadmin

密码：111111


## 技术栈

### 服务端
| 框架             | 备注           | 版本          |
| ---------------- | -------------- | ------------- |
| Spring  Boot     | 核心框架       | 2.0.1.RELEASE |
| mybatis  plus    | 持久框架       | 3.4.2         |
| sharding  sphere | 分库分表       | 4.1.0         |
| Maven            | 程序构建       |               |
| Mysql            | 数据库         | 5.6/5.7       |
| RabbitMQ         | 消息中间件AMQP | 3.x(3.6.14)   |
| Redis            | 缓存           | 5.x           |
| Spring Security  | 安全框架       | 2.0.1.RELEASE |
| Druid            | 数据库连接池   | 1.1.22        |
| xxl-job          | 定时任务       | 2.0.0         |
### 客户端
| 框架              | 备注         | 版本   |
| ----------------- | ------------ | ------ |
| webpack           | 构建工具     | 3.10.0 |
| ES6               | JS版本       |        |
| Vue.js            | 基础JS框架   | 2.6.14 |
| jQuery            | 辅助JS库     | 2.1.4  |
| Vue Router        | 路由管理     | 3.0.1  |
| Vuex              | 状态管理     | 3.0.1  |
| Element UI        | 基础UI库     | 2.15.5 |
| vue-element-admin | UI界面基于   |        |
| Axios             | 网络请求     | 0.18.0 |
| Scss              | CSS预处理    | 4.13.0 |
| ESLint            | 代码检查     | 4.13.1 |
| ECharts           | 报表系统     | 3.8.5  |
| 百度 UEditor      | 富文本编辑器 |        |
| 百度 Web Uploader | 图片上传插件 |        |

## 功能介绍
| 功能   | 描述                                                                                           |
|------|----------------------------------------------------------------------------------------------|
| 采购管理 | 包括采购计划、合同管理、采购入库、供应商退货、供应商结算等采购相关模块，管理员可根据采购计划以及和三方签订的采购合同进行采购入库，和供应商根据采购入库单进行结算。            | 
| 仓库管理 | 包括商品调拨、商品借出、商品换货、库存盘点、库存报损、库存预警等库存相关模块，管理员可以将商品在不同的仓库之间进行调拨，对单个仓库商品进行盘点，对需要商品进行报损和查看库存预警的商品。 |
| 库存报表 | 包括库存统计、库存成本统计、总入库统计、供应商退货统计、出库统计、订单退货统计、调拨统计、库存调整统计、商品换货统计等库存变动报表。                           |
| 销售管理 | 包括订单列表、出库管理、订单退货模块，管理员可以创建销售订单，对订单进行发货、付款处理，也可对订单进行退货处理。                                     |
| 商品管理 | 包括商品档案、规格管理、品牌管理模块，管理员新增/编辑商品基础信息，维护库存预警数量，管理商品分类、规格参数、品牌等信息。                                |
| 基础设置 | 包括供应商管理、营销经理管理、仓库管理、编号规则设置、会员管理、支付方式管理、地区管理、物流公司管理、门店管理、运费模板模块。                              |
| 系统管理 | 包括系统设置、存储方案、数据字典、日志管理、openapi秘钥配置、人员管理、权限管理、菜单维护、部门管理、岗位管理模块。                                |

## 效果图
![首页](https://hys-project.oss-cn-beijing.aliyuncs.com/erp/%E9%A6%96%E9%A1%B5.png)
![采购入库](https://hys-project.oss-cn-beijing.aliyuncs.com/erp/%E9%87%87%E8%B4%AD%E5%85%A5%E5%BA%93.png)
![订单退货](https://hys-project.oss-cn-beijing.aliyuncs.com/erp/%E8%AE%A2%E5%8D%95%E9%80%80%E8%B4%A7.png)
![订单列表](https://hys-project.oss-cn-beijing.aliyuncs.com/erp/%E8%AE%A2%E5%8D%95%E5%88%97%E8%A1%A8.png)
![新增商品](https://hys-project.oss-cn-beijing.aliyuncs.com/erp/%E6%96%B0%E5%A2%9E%E5%95%86%E5%93%81.png)
![库存盘点](https://hys-project.oss-cn-beijing.aliyuncs.com/erp/%E5%BA%93%E5%AD%98%E7%9B%98%E7%82%B9.png)
![商品调拨](https://hys-project.oss-cn-beijing.aliyuncs.com/erp/%E5%95%86%E5%93%81%E8%B0%83%E6%8B%A8.png)
![商品列表](https://hys-project.oss-cn-beijing.aliyuncs.com/erp/%E5%95%86%E5%93%81%E5%88%97%E8%A1%A8.png)
![创建订单](https://hys-project.oss-cn-beijing.aliyuncs.com/erp/%E5%88%9B%E5%BB%BA%E8%AE%A2%E5%8D%95.png)
![仓库管理](https://hys-project.oss-cn-beijing.aliyuncs.com/erp/%E4%BB%93%E5%BA%93%E7%AE%A1%E7%90%86.png)
![供应商管理](https://hys-project.oss-cn-beijing.aliyuncs.com/erp/%E4%BE%9B%E5%BA%94%E5%95%86%E7%AE%A1%E7%90%86.png)
![供应商结算](https://hys-project.oss-cn-beijing.aliyuncs.com/erp/%E4%BE%9B%E5%BA%94%E5%95%86%E7%BB%93%E7%AE%97.png)
![供应商退货](https://hys-project.oss-cn-beijing.aliyuncs.com/erp/%E4%BE%9B%E5%BA%94%E5%95%86%E9%80%80%E8%B4%A7.png)
![入库统计](https://hys-project.oss-cn-beijing.aliyuncs.com/erp/%E5%85%A5%E5%BA%93%E5%95%86%E5%93%81%E7%BB%9F%E8%AE%A1.png)

## 使用须知

1. 允许个人学习使用。
2. 允许用于学习、毕业设计等。
3. 禁止将本开源的代码和资源进行任何形式任何名义的出售。
4. 限制商业使用，如需联系QQ：2025555598 或微信扫一扫加我微信。

   ![](https://shoptnt-bbc.oss-cn-beijing.aliyuncs.com/tnt/12261709021247_.pic.jpg?x-oss-process=style/300x300)

## 交流、反馈

### 推荐
官方QQ群：
<a target="_blank" href="https://qm.qq.com/q/JLXlUb0GMa"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="magicErp开源交流群" title="magicErp开源交流群"></a> 827733509
