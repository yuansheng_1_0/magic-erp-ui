/**
 * Created by Andste on 2021/8/18.
 * 采购计划选择器
 * 依赖于element-ui
 */
import Vue from 'vue'
import PickerContract from './src/main.vue'
import PickerContractPicker from './src/main.js'

PickerContract.install = function() {
  Vue.component(PickerContract.name, PickerContract)
}
Vue.prototype.$EnPickerContract = PickerContractPicker

export default PickerContract
