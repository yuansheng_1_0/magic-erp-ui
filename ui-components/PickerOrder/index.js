/**
 * Created by Andste on 2021/8/18.
 * 订单编号选择器
 * 依赖于element-ui
 */
import Vue from 'vue'
import PickerOrder from './src/main.vue'
import PickerOrderPicker from './src/main.js'

PickerOrder.install = function() {
  Vue.component(PickerOrder.name, PickerOrder)
}
Vue.prototype.$EnPickerOrder = PickerOrderPicker

export default PickerOrder
