/**
 * Created by Andste on 2021/8/18.
 * 采购计划选择器
 * 依赖于element-ui
 */
import Vue from 'vue'
import warehouseEntryBatch from './src/main.vue'
import PickerWarehouseEntryBatch from './src/main.js'

warehouseEntryBatch.install = function() {
  Vue.component(warehouseEntryBatch.name, warehouseEntryBatch)
}
Vue.prototype.$EnwarehouseEntryBatch = PickerWarehouseEntryBatch

export default warehouseEntryBatch
